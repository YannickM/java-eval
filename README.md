# java-eval

TP de fin de session "Java", avant d'attaquer le JEE !

## Faisons un zoo !

Nous souhaitons gérer la population d'un Zoo.
Dans ce zoo, nous avons des animaux, déclinés sous plusieurs sortes:
- Mammifères,
- oiseaux,
- poissons,
- reptiles.

Ces animaux peuvent être vivipares ou ovipares (Un ovipare pond et un vivipare "met bas")

### Définition

Un animal dans notre zoo est caractérisé par :
- un petit nom
- s'il a un système respiratoire
- un type d'alimentation
- s'il a une reproduction sexuée ou non
- espérance de vie
- zone de vie
- une espèce

Un animal est forcément un mammifère, un oiseau, un poisson ou un reptile.

Un **mammifère** est notamment caractérisé par :
- une période d'allaitement
- type de locomotion (quadrupédie, bipédie, vol battu, nage...)

La plupart des mammifères sont vivipares, sauf deux qui sont ovipares (l'ornithorynque et l'échidné)

Un **oiseau** est caractérisé par :
- une capacité à voler ou non
- forme des pattes (anisodactyle, zygodactyle, ... )
- nombre moyen d'œufs par couvées

Tous les oiseaux sont ovipares.

Un **poisson** est caractérisé par :
- type d'eau (eau douce, mer, ...)
- sang froid (oui/non)

Tous les oiseaux sont ovipares.

Enfin, un **reptile** possède les caractéristiques suivantes :
- mue (oui/non)
- milieu (terrestre/aquatique)

Il peut y avoir des reptiles ovipares (Iguanes, python) et vivipares: (anaconda, boa)


### D'un point de vue POO ?

Un animal est obligatoirement une espèce spécifique: Animal est donc une
classe abstraite qui regroupe des notions communes mais nécessite d'être spécifiée.
Mammifère, Oiseau, Poisson et Reptile sont des classes concrètes.

De plus, ces animaux sont être des Ovipare ou Vivipare. L'héritage
multiple n'étant pas envisageable, Ovipare et Vivipare sont des interfaces,
avec chacune une opération (`Ovipare#pondre()` et `Vivipare#mettreBas()` ).


## Sujet

Il y a beaucoup de données, et nous allons avancer par étapes. Considérons
que nous faisons notre Zoo zone par zone.  
Nous allons démarrer par les oiseaux.

### Consigne de projet

Vous commencerez par cloner le projet et faire une branche à votre nom.
Dans l'idéal, vous ferez des commit réguliers (au minimum un par étape
serait bien).
Une fois sur votre branche, vous pouvez ouvrir le projet et démarrer !

Dans Intellij, ouvrez le fichier pom.xml du projet, et choississez de l'ouvrir 'en tant que projet'.

### Étape 1 : un zoo et des animaux

La première étape consiste à décire de manière générale ce que nous avons.

Créez la classe abstraite `Animal`, avec les attibuts suivant :
- nom : String
- systRespiratoire : boolean
- alimentation: String
- reproductionSexuee : boolean
- esperanceDeVie : int
- zone : String

Créer la classe `Zoo`. Elle a les attributs suivants :
- nom : String
- adresse : String
- animaux : List< Animal >

À cette étape, nous avons donc la possibilité de faire un Zoo qui
contiendra toute variété d'animaux.

### Étape 2 : des animaux concrets

Nous allons nous intéresser aux oiseaux de notre Zoo.

Avant tout, il faut faire la classe `Oiseau` regroupant les
caractéristiques communes:
- peutVoler : boolean
- formePattes : String
- nbOeufs : int

De plus, les oiseaux sont ovipares : la classe `Oiseau` met en œuvre
l'interface Ovipare.

Créer l'interface `Ovipare` requérant la méthode `void pond()`.

Faites que la classe `Oiseau` "implements" `Ovipare` et affiche dans la
méthode `pond()` "< nom > pond en moyenne < nbOeufs > œufs."


### Étape 3 : un zoo qui se peuple

Maintenant que nous avons une classe concrète, on peut ajouter des oiseaux
dans notre Zoo !

Pour rappel, du fait de l'héritage, les oiseaux ont toutes les
caractéristiques décrites dans Animal.

Ajoutez une méthode dans la classe `Zoo` permettant de lui ajouter un `Oiseau`.


### Étape 4 : Un peu de mammifère ...

On s'attaque désormais aux mammifères. Nous en avons finallement deux
sortes : les vivipares et des ovipares.

Pour le moment, nous n'avons fait que l'interface `Ovipare`, nous pouvons
ajouter une nouvelle interface, `Vivipare` qui permet de contraindre la
méthode `void metBas()`.

Il nous faut donc au moins deux classes : une pour les mammifères
vivipares (avec le `implements Vivipare`) qui affiche dans la méthode
`metBas()` le message "< nom > met bas" , et une pour les mamminfères
ovipares (avec le `implements Ovipare`) qui affiche dans la méthode
`pond()` le message "< nom > pond des oeufs".

On se retrouve donc avec les classes :
- MammifereOvipare :
  - moisAllaitement : int
  - locomotion : String
  - et une méthode `metBas()`
- MammifereVivipare :
  - moisAllaitement : int
  - locomotion : String
  - et une méthode `pond()`

